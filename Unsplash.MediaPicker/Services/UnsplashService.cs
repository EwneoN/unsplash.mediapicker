﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Unsplash.MediaPicker.Core;
using Unsplash.MediaPicker.Core.Models;
using Collection = Unsplash.MediaPicker.Core.Models.Collection;
using Photo = Unsplash.MediaPicker.Core.Models.Photo;
using User = Unsplash.MediaPicker.Core.Models.User;

namespace Unsplash.MediaPicker.Services
{
	public class UnsplashService
	{
		private readonly UnsplashClient _Client;

		public UnsplashService(UnsplashClient client)
		{
			_Client = client ?? throw new ArgumentNullException(nameof(client));
		}
		
		public async Task<Collection[]> GetCollections(int page, int pageSize)
		{
			var collections = await _Client.GetCollections(page, pageSize);

			return collections.Select(TransformCollection).ToArray();
		}

		public async Task<Collection[]> GetFeaturedCollections(int page, int pageSize)
		{
			var collections = await _Client.GetFeaturedCollections(page, pageSize);

			return collections.Select(TransformCollection).ToArray();
		}

		public async Task<Collection[]> GetRelatedCollections(int collectionId)
		{
			var collections = await _Client.GetRelatedCollections(collectionId);

			return collections.Select(TransformCollection).ToArray();
		}

		public async Task<Photo[]> GetCollectionPhotos(int collectionId, int page, int pageSize)
		{
			var collections = await _Client.GetCollectionPhotos(collectionId, page, pageSize);

			return collections.Select(TransformPhoto).ToArray();
		}

		public async Task<Photo[]> GetUserPhotos(string username, int page, int pageSize, OrderPhotosBy orderBy)
		{
			var collections = await _Client.GetUserPhotos(username, page, pageSize, orderBy);

			return collections.Select(TransformPhoto).ToArray();
		}

		public async Task<PagedResult<Photo>> SearchPhotos(string query, int page, int pageSize, PhotoOrientation orientation, int[] collectionIds)
		{
			var result = await _Client.SearchPhotos(query, page, pageSize, orientation, collectionIds);

			return new PagedResult<Photo>(result.total, page, pageSize)
			{
				Items = result.results.Select(TransformPhoto).ToArray()
			};
		}

		public async Task<PagedResult<Photo>> SearchCollectionPhotos(string query, int collectionId, int page, int pageSize, PhotoOrientation orientation)
		{
			var result = await _Client.SearchPhotos(query, page, pageSize, orientation, collectionId);

			return new PagedResult<Photo>(result.total, page, pageSize)
			{
				Items = result.results.Select(TransformPhoto).ToArray()
			};
		}

		public async Task<PagedResult<Collection>> SearchCollections(string query, int page, int pageSize)
		{
			var result = await _Client.SearchCollections(query, page, pageSize);

			return new PagedResult<Collection>(result.total, page, pageSize)
			{
				Items = result.results.Select(TransformCollection).ToArray()
			};
		}

		public async Task<PagedResult<User>> SearchUsers(string query, int page, int pageSize)
		{
			var result = await _Client.SearchUsers(query, page, pageSize);

			return new PagedResult<User>(result.total, page, pageSize)
			{
				Items = result.results.Select(TransformUser).ToArray()
			};
		}

		private User TransformUser(Core.Models.Api.User user)
		{
			return new User
			{
				Id = user.id,
				Username = user.username,
				FirstName = user.first_name,
				LastName = user.last_name,
				Name = user.name,
				ProfileUrl = user.links.html,
				ProfileImageSmallUrl = user.profile_image.small,
				ProfileImageMediumUrl = user.profile_image.medium,
				ProfileImageLargeUrl= user.profile_image.large
			};
		}

		private Collection TransformCollection(Core.Models.Api.Collection collection)
		{
			return new Collection
			{
				Id = collection.id,
				Title = collection.title,
				Description = collection.description,
				CoverPhotoUrl = collection.cover_photo?.urls.raw,
				FriendlyUrl = collection.links.html,
				TotalPhotos = collection.total_photos
			};
		}

		private Photo TransformPhoto(Core.Models.Api.Photo photo)
		{
			return new Photo
			{
				Id = photo.id,
				Description = photo.description,
				Width = photo.width,
				Height = photo.height,
				RawUrl = photo.urls.raw,
				SmallUrl = photo.urls.raw,
				RegularUrl = photo.urls.regular,
				FullUrl = photo.urls.full,
				ThumbnailUrl = photo.urls.thumb
			};
		}
	}
}
