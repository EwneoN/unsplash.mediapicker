﻿(function() {
	"use strict";

	function unsplashMediaPickerApi($http, umbRequestHelper) {
		function getCollections(page, pageSize) {
			return umbRequestHelper.resourcePromise(
				$http.get("/umbraco/backoffice/api/UnsplashMediaPicker/getCollections", {
					params: {
						page: page,
						pageSize: pageSize
					}
				}),
				"Failed to get collections"
			);
		}

		function getFeaturedCollections(page, pageSize) {
			return umbRequestHelper.resourcePromise(
				$http.get("/umbraco/backoffice/api/UnsplashMediaPicker/getFeaturedCollections", {
					params: {
						page: page,
						pageSize: pageSize
					}
				}),
				"Failed to get featured collections"
			);
		}

		function getRelatedCollections(collectionId, page, pageSize) {
			return umbRequestHelper.resourcePromise(
				$http.get("/umbraco/backoffice/api/UnsplashMediaPicker/getRelatedCollections", {
					params: {
						collectionId: collectionId,
						page: page,
						pageSize: pageSize
					}
				}),
				"Failed to get related collections"
			);
		}

		function getCollectionPhotos(collectionId, page, pageSize) {
			return umbRequestHelper.resourcePromise(
				$http.get("/umbraco/backoffice/api/UnsplashMediaPicker/getCollectionPhotos", {
					params: {
						collectionId: collectionId,
						page: page,
						pageSize: pageSize
					}
				}),
				"Failed to get collection photos"
			);
		}

		function getUserPhotos(username, page, pageSize, orderBy) {
			return umbRequestHelper.resourcePromise(
				$http.get("/umbraco/backoffice/api/UnsplashMediaPicker/getUserPhotos", {
					params: {
						username: username,
						page: page,
						pageSize: pageSize,
						orderBy: orderBy
					}
				}),
				"Failed to get collections"
			);
		}

		function searchPhotos(query, page, pageSize, orientation, collectionIds) {
			return umbRequestHelper.resourcePromise(
				$http.get("/umbraco/backoffice/api/UnsplashMediaPicker/searchPhotos", {
					params: {
						query: query,
						page: page,
						pageSize: pageSize,
						orientation: orientation,
						collectionIds: collectionIds
					}
				}),
				"Failed to get photos"
			);
		}

		function searchCollections(query, page, pageSize) {
			return umbRequestHelper.resourcePromise(
				$http.get("/umbraco/backoffice/api/UnsplashMediaPicker/searchCollections", {
					params: {
						query: query,
						page: page,
						pageSize: pageSize
					}
				}),
				"Failed to get collections"
			);
		}

		function searchUsers(query, page, pageSize) {
			return umbRequestHelper.resourcePromise(
				$http.get("/umbraco/backoffice/api/UnsplashMediaPicker/searchUsers", {
					params: {
						query: query,
						page: page,
						pageSize: pageSize
					}
				}),
				"Failed to get users"
			);
		}

		return {
			getCollections: getCollections,
			getFeaturedCollections: getFeaturedCollections,
			getRelatedCollections: getRelatedCollections,
			getCollectionPhotos: getCollectionPhotos,
			getUserPhotos: getUserPhotos,
			searchPhotos: searchPhotos,
			searchCollections: searchCollections,
			searchUsers: searchUsers
		}
	}

	angular
		.module("umbraco.resources")
		.factory("unsplashMediaPickerApi", unsplashMediaPickerApi);
})();