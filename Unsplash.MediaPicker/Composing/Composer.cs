﻿using System.Configuration;
using Umbraco.Core.Composing;
using Unsplash.MediaPicker.Core;
using Unsplash.MediaPicker.Services;

namespace Unsplash.MediaPicker.Composing
{
	public class Composer : IUserComposer
	{
		public void Compose(Composition composition)
		{
			var appSecret = ConfigurationManager.AppSettings["Unsplash.MediaPicker.AppSecret"];

			if (string.IsNullOrWhiteSpace(appSecret))
			{
				return;
			}

			composition.RegisterUnique(f =>
			{
				var client = new UnsplashClient(new UnsplashClientConfig
				{
					AccessKey = appSecret
				});

				return new UnsplashService(client);
			});
		}
	}
}
