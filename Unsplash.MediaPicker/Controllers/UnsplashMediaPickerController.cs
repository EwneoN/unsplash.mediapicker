﻿using System;
using System.Web.Http;
using Umbraco.Web.WebApi;
using Unsplash.MediaPicker.Core.Models;
using Unsplash.MediaPicker.Services;

namespace Unsplash.MediaPicker.Controllers
{
	public class UnsplashMediaPickerController : UmbracoAuthorizedApiController
	{
		private readonly UnsplashService _UnsplashService;

		public UnsplashMediaPickerController(UnsplashService unsplashService)
		{
			_UnsplashService = unsplashService ?? throw new ArgumentNullException(nameof(unsplashService));
		}

		public IHttpActionResult GetCollections(int page, int pageSize)
		{
			if (page <= 0)
			{
				return BadRequest("page must be greater than 0");
			}

			if (pageSize <= 0)
			{
				return BadRequest("pageSize must be greater than 0");
			}

			var collections = _UnsplashService.GetCollections(page, pageSize);

			return Json(collections);
		}

		public IHttpActionResult GetFeaturedCollections(int page, int pageSize)
		{
			if (page <= 0)
			{
				return BadRequest("page must be greater than 0");
			}

			if (pageSize <= 0)
			{
				return BadRequest("pageSize must be greater than 0");
			}

			var collections = _UnsplashService.GetFeaturedCollections(page, pageSize);

			return Json(collections);
		}

		public IHttpActionResult GetRelatedCollections(int collectionId)
		{
			if (collectionId <= 0)
			{
				return BadRequest("collectionId is required");
			}

			var collections = _UnsplashService.GetRelatedCollections(collectionId);

			return Json(collections);
		}

		public IHttpActionResult GetCollectionPhotos(int collectionId, int page, int pageSize)
		{
			if (collectionId <= 0)
			{
				return BadRequest("collectionId is required");
			}

			if (page <= 0)
			{
				return BadRequest("page must be greater than 0");
			}

			if (pageSize <= 0)
			{
				return BadRequest("pageSize must be greater than 0");
			}

			var photos = _UnsplashService.GetCollectionPhotos(collectionId, page, pageSize);

			return Json(photos);
		}

		public IHttpActionResult GetUserPhotos(string username, int page, int pageSize, OrderPhotosBy orderBy)
		{
			if (string.IsNullOrWhiteSpace(username))
			{
				return BadRequest("username is required");
			}

			if (page <= 0)
			{
				return BadRequest("page must be greater than 0");
			}

			if (pageSize <= 0)
			{
				return BadRequest("pageSize must be greater than 0");
			}

			var photos = _UnsplashService.GetUserPhotos(username, page, pageSize, orderBy);

			return Json(photos);
		}

		public IHttpActionResult SearchPhotos(string query, int page, int pageSize, PhotoOrientation orientation, int[] collectionIds = null)
		{
			if (string.IsNullOrWhiteSpace(query))
			{
				return BadRequest("query is required");
			}

			if (page <= 0)
			{
				return BadRequest("page must be greater than 0");
			}

			if (pageSize <= 0)
			{
				return BadRequest("pageSize must be greater than 0");
			}

			var photos = _UnsplashService.SearchPhotos(query, page, pageSize, orientation, collectionIds);

			return Json(photos);
		}

		public IHttpActionResult SearchCollections(string query, int page, int pageSize)
		{
			if (string.IsNullOrWhiteSpace(query))
			{
				return BadRequest("query is required");
			}

			if (page <= 0)
			{
				return BadRequest("page must be greater than 0");
			}

			if (pageSize <= 0)
			{
				return BadRequest("pageSize must be greater than 0");
			}

			var collections = _UnsplashService.SearchCollections(query, page, pageSize);

			return Json(collections);
		}

		public IHttpActionResult SearchUsers(string query, int page, int pageSize)
		{
			if (string.IsNullOrWhiteSpace(query))
			{
				return BadRequest("query is required");
			}

			if (page <= 0)
			{
				return BadRequest("page must be greater than 0");
			}

			if (pageSize <= 0)
			{
				return BadRequest("pageSize must be greater than 0");
			}

			var users = _UnsplashService.SearchUsers(query, page, pageSize);

			return Json(users);
		}
	}
}
