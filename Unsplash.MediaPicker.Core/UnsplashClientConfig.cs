﻿namespace Unsplash.MediaPicker.Core
{
	public class UnsplashClientConfig
	{
		public string AccessKey { get; set; }
	}
}