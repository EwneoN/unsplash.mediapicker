﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Unsplash.MediaPicker.Core.Models;
using Unsplash.MediaPicker.Core.Models.Api;
using Collection = Unsplash.MediaPicker.Core.Models.Api.Collection;
using Photo = Unsplash.MediaPicker.Core.Models.Api.Photo;
using User = Unsplash.MediaPicker.Core.Models.Api.User;

namespace Unsplash.MediaPicker.Core
{
	public class UnsplashClient
	{
		private const string SearchUrl = "https://api.unsplash.com/search";
		private const string PhotosUrl = "https://api.unsplash.com/photos";
		private const string CollectionsUrl = "https://api.unsplash.com/collections";
		private const string UsersUrl = "https://api.unsplash.com/users";

		private readonly string _AccessKey;

		public UnsplashClient(UnsplashClientConfig config)
		{
			if (config == null)
			{
				throw new ArgumentNullException(nameof(config));
			}

			if (string.IsNullOrWhiteSpace(config.AccessKey))
			{
				throw new ArgumentException("AccessKey is required", nameof(config.AccessKey));
			}

			_AccessKey = config.AccessKey;
		}

		public async Task<Photo> GetPhoto(string photoId)
		{
			if (string.IsNullOrWhiteSpace(photoId))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(photoId));
			}

			return await HttpGet<Photo>($"{PhotosUrl}/{photoId}");
		}

		public async Task<Photo[]> GetPhotos(int page, int pageSize, OrderPhotosBy orderBy = OrderPhotosBy.latest)
		{
			if (page <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(page));
			}

			if (pageSize <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(pageSize));
			}

			return await HttpGet<Photo[]>($"{PhotosUrl}?page={page}&per_page={pageSize}&order_by={orderBy}");
		}

		public async Task<Photo[]> GetCollectionPhotos(int collectionId, int page, int pageSize)
		{
			if (collectionId <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(collectionId));
			}

			if (page <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(page));
			}

			if (pageSize <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(pageSize));
			}

			return await HttpGet<Photo[]>($"{CollectionsUrl}/{collectionId}/photos?page={page}&per_page={pageSize}");
		}

		public async Task<Photo[]> GetUserPhotos(string username, int page, int pageSize, OrderPhotosBy orderBy = OrderPhotosBy.latest)
		{
			if (string.IsNullOrWhiteSpace(username))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(username));
			}

			if (page <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(page));
			}

			if (pageSize <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(pageSize));
			}

			return await HttpGet<Photo[]>($"{UsersUrl}/{username}/photos?page={page}&per_page={pageSize}&order_by={orderBy}");
		}

		public async Task<SearchResults<Photo>> SearchPhotos(string query, int page, int pageSize,
		                                                     PhotoOrientation orientation, params int[] collectionIds)
		{
			if (page <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(page));
			}

			if (pageSize <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(pageSize));
			}

			if (string.IsNullOrWhiteSpace(query))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(query));
			}

			return await Search<Photo>(query, page, pageSize, collectionIds, orientation);
		}

		public async Task<Collection> GetCollection(int collectionId)
		{
			if (collectionId <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(collectionId));
			}

			return await HttpGet<Collection>($"{CollectionsUrl}/{collectionId}");
		}

		public async Task<Collection[]> GetCollections(int page, int pageSize)
		{
			if (page <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(page));
			}

			if (pageSize <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(pageSize));
			}

			return await HttpGet<Collection[]>($"{CollectionsUrl}?page={page}&per_page={pageSize}");
		}

		public async Task<Collection[]> GetFeaturedCollections(int page, int pageSize)
		{
			if (page <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(page));
			}

			if (pageSize <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(pageSize));
			}

			return await HttpGet<Collection[]>($"{CollectionsUrl}/featured?page={page}&per_page={pageSize}");
		}

		public async Task<Collection[]> GetRelatedCollections(int collectionId)
		{
			if (collectionId <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(collectionId));
			}

			return await HttpGet<Collection[]>(
				       $"{CollectionsUrl}/{collectionId}/related");
		}
		
		public async Task<Collection[]> GetUsersCollections(string username, int page, int pageSize)
		{
			if (string.IsNullOrWhiteSpace(username))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(username));
			}

			if (page <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(page));
			}

			if (pageSize <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(pageSize));
			}

			return await HttpGet<Collection[]>($"{UsersUrl}/{username}/collections?page={page}&per_page={pageSize}");
		}

		public async Task<SearchResults<Collection>> SearchCollections(string query, int page, int pageSize)
		{
			if (page <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(page));
			}

			if (pageSize <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(pageSize));
			}

			if (string.IsNullOrWhiteSpace(query))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(query));
			}

			return await Search<Collection>(query, page, pageSize);
		}
		
		public async Task<SearchResults<User>> SearchUsers(string query, int page, int pageSize)
		{
			if (page <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(page));
			}

			if (pageSize <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(pageSize));
			}

			if (string.IsNullOrWhiteSpace(query))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(query));
			}

			return await Search<User>(query, page, pageSize);
		}

		private async Task<TResponse> HttpGet<TResponse>(string requestUrl)
		{
			using (HttpClient client = new HttpClient())
			{
				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Client-ID", _AccessKey);
				string responseString = await client.GetStringAsync(requestUrl);

				return JsonConvert.DeserializeObject<TResponse>(responseString);
			}
		}

		private async Task<SearchResults<TResult>> Search<TResult>(string query, int page, int pageSize,
		                                                           int[] collectionIds = null,
		                                                           PhotoOrientation? orientation = null)
		{
			string searchType;
			StringBuilder queryString = new StringBuilder($"?query={query}&page={page}&per_page={pageSize}");

			if (typeof(TResult) == typeof(Collection))
			{
				searchType = "collections";
			}
			else if (typeof(TResult) == typeof(User))
			{
				searchType = "users";
			}
			else
			{
				searchType = "photos";
			}

			if (collectionIds != null && collectionIds.Length >= 1)
			{
				string collectionParam = string.Join(",", collectionIds);
				queryString.Append($"&collections={collectionParam}");
			}

			if (orientation.HasValue)
			{
				queryString.Append($"&orientation={orientation}");
			}

			return await HttpGet<SearchResults<TResult>>($"{SearchUrl}/{searchType}{queryString}");
		}
	}
}