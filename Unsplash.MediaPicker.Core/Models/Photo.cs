﻿using System;

namespace Unsplash.MediaPicker.Core.Models
{
	public class Photo
	{
		public string Id { get; set; }
		public string Description { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public string RawUrl { get; set; }
		public string FullUrl { get; set; }
		public string RegularUrl { get; set; }
		public string SmallUrl { get; set; }
		public string ThumbnailUrl { get; set; }
		private DateTimeOffset CreatedAt { get; set; }
		private DateTimeOffset? UpdatedAt { get; set; }
	}
}