﻿namespace Unsplash.MediaPicker.Core.Models
{
	public enum OrderPhotosBy
	{
		latest,
		oldest,
		popular
	}
}
