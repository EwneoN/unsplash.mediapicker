﻿namespace Unsplash.MediaPicker.Core.Models
{
	public enum PhotoOrientation
	{
		landscape,
		portrait,
		squarish
	}
}