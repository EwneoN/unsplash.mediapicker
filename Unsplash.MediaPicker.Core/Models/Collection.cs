﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unsplash.MediaPicker.Core.Models
{
	public class Collection
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string CoverPhotoUrl { get; set; }
		public int TotalPhotos { get; set; }
		public string FriendlyUrl { get; set; }
		private DateTimeOffset PublishedAt { get; set; }
		private DateTimeOffset? UpdatedAt { get; set; }
	}
}
