﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unsplash.MediaPicker.Core.Models
{
	public class PickedPhoto
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public string RawUrl { get; set; }
		public string ThumbnailUrl { get; set; }
	}
}
