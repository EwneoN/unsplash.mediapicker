﻿namespace Unsplash.MediaPicker.Core.Models.Api
{
	public class SearchResults<TResultItem>
	{
		public int total { get; set; }
		public int total_pages { get; set; }
		public TResultItem[] results { get; set; }
	}
}
