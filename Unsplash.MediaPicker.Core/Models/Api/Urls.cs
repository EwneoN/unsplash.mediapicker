﻿namespace Unsplash.MediaPicker.Core.Models.Api
{
	public class Urls
	{
		public string raw { get; set; }
		public string full { get; set; }
		public string regular { get; set; }
		public string small { get; set; }
		public string thumb { get; set; }
	}
}