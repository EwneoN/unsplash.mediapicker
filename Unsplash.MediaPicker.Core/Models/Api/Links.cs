﻿namespace Unsplash.MediaPicker.Core.Models.Api
{
	public class Links
	{
		public string self { get; set; }
		public string html { get; set; }
		public string photos { get; set; }
		public string related { get; set; }
		public string likes { get; set; }
		public string portfolio { get; set; }
		public string download { get; set; }
		public string download_location { get; set; }
	}
}