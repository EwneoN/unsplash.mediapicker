﻿namespace Unsplash.MediaPicker.Core.Models.Api
{
	public class ProfileImage
	{
		public string small { get; set; }
		public string medium { get; set; }
		public string large { get; set; }
	}
}