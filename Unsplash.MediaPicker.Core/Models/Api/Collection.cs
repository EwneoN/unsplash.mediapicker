﻿using System;

namespace Unsplash.MediaPicker.Core.Models.Api
{
	public class Collection
	{
		public int id { get; set; }
		public string title { get; set; }
		public string description { get; set; }
		public DateTime published_at { get; set; }
		public DateTime updated_at { get; set; }
		public int total_photos { get; set; }
		public bool _private { get; set; }
		public string share_key { get; set; }
		public Photo cover_photo { get; set; }
		public User user { get; set; }
		public Links links { get; set; }
	}
}