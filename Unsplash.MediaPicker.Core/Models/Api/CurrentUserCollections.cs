﻿using System;

namespace Unsplash.MediaPicker.Core.Models.Api
{
	public class CurrentUserCollections
	{
		public int id { get; set; }
		public string title { get; set; }
		public DateTime published_at { get; set; }
		public DateTime updated_at { get; set; }
		public object cover_photo { get; set; }
		public object user { get; set; }
	}
}