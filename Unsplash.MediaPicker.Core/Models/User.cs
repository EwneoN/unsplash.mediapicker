﻿namespace Unsplash.MediaPicker.Core.Models
{
	public class User
	{
		public string Id { get; set; }
		public string Username { get; set; }
		public string Name { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string ProfileUrl { get; set; }
		public string ProfileImageSmallUrl { get; set; }
		public string ProfileImageMediumUrl { get; set; }
		public string ProfileImageLargeUrl { get; set; }
	}
}